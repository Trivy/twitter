# ExoDataMining ===
# Un exercice en Data Mining
# Première partie : listener
#
# À utiliser grâce à :
# python twitter_streaming.py > ../data/stream_test.txt
#
# Thanks to: http://adilmoujahid.com/posts/2014/07/twitter-analytics/

#Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream

#Variables that contains the user credentials to access Twitter API 
access_token = "870991357225168896-WeT9Sx7i3ilDjfV3gmWSp8PPnOEEryw"
access_token_secret = "Uabei6PvseOJHEULzoI7ofw43P87D3CxAquWGf2edWRyo"
consumer_key = "ZqHiEOsWcHqotNXf6CoTiz2gN"
consumer_secret = "SM8MetFIdGy2d9Pf5VqrvyRPh8Z12jsNQG3avLWjQl872MngrY"

#This is a basic listener that just prints received tweets to stdout.
class StdOutListener(StreamListener):

    def on_data(self, data):
        print(data)
        return True

    def on_error(self, status):
        print("Erreur !")
        print(status)


if __name__ == '__main__':

    #This handles Twitter authentification and the connection to Twitter Streaming API
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)

    #This line filter Twitter Streams to capture data by the keywords in the list
    listKeywords = ['#a380','a380','@a380fanclub']
    stream.filter(track=listKeywords)