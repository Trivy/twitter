# ExoDataMining
# Un exercice en Data Mining sur Twitter
# Deuxième partie : traitement
#
# Basé sur http://adilmoujahid.com/posts/2014/07/twitter-analytics/

import json
import pandas as pd
import matplotlib.pyplot as plt
import re
import networkx as nx

PATH = 'E:\\Data\\Twitter\\'
tweets_data_path = PATH+'data\\stream_a380v5.txt'

tweets_data = []
tweets_file = open(tweets_data_path, "r")
for line in tweets_file:
    try:
        tweet = json.loads(line)
        tweets_data.append(tweet)
    except:
        continue
    
data_ini = [ [ tweet.get('text'), tweet.get('lang'), tweet.get('place')['country'] if tweet.get('place') != None else None ] for tweet in tweets_data ]

tweets = pd.DataFrame(data = data_ini, columns=['text','lang', 'country'])

## Counting tweets by language:
tweets_by_lang = tweets['lang'].value_counts()

fig, ax = plt.subplots()
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=10)
ax.set_xlabel('Languages', fontsize=15)
ax.set_ylabel('Number of tweets' , fontsize=15)
ax.set_title('Top 5 languages', fontsize=15, fontweight='bold')
tweets_by_lang[:5].plot(ax=ax, kind='bar', color='red')

plt.show()

## Counting tweets by country:
tweets_by_country = tweets['country'].value_counts()

fig, ax = plt.subplots()
ax.tick_params(axis='x', labelsize=15)
ax.tick_params(axis='y', labelsize=10)
ax.set_xlabel('Country', fontsize=15)
ax.set_ylabel('Number of tweets' , fontsize=15)
ax.set_title('Top 5 countries', fontsize=15, fontweight='bold')
tweets_by_country[:5].plot(ax=ax, kind='bar', color='blue')

plt.show()

## Extract hashtags

# Create a dictionnary with keys: all hashtags and value: the list of occurences
dictHashtags = {}
i = 0
for tweetText in tweets['text']:
    match = re.findall('\#\S+',tweetText)
    if match:
        for word in match:
            # removes '…' and other punctation signs if they are the last characters of word 
            # and take the lowercase version
            word=word.rstrip('…,!?.').lower()
            try:
                dictHashtags[word].add(i)
            except KeyError:
                dictHashtags[word]=set([i])
    i+=1

# Dictionnary of hashtags with the number of occurences:
nbrOccurrences = {key:len(dictHashtags[key]) for key in dictHashtags.keys()}

# Same, but sorted (most frequent first)
sortedOcc = sorted( nbrOccurrences.items(), key = lambda x: x[1], reverse=True)

# Keep only hashtags with largest occurrences
dictLabels = { key:key for key in nbrOccurrences.keys() if nbrOccurrences[key]>7 }

## dictionnary of edges:
# pairTweets = { (key1, key2): set1.intersection(set2) for key1,set1 in dictHashtags.items() for key2,set2 in dictHashtags.items() if (key1 !=key2 and bool( set1.intersection(set2))) }
# # NB: symmetric dictionnary: both pairTweets[(key1,key2)] and pairTweets[(key2,key1)] exist (and yield the same result, hopefully!)
# # It keeps only the pairs of keys with nontrivial intersection.

## Co-occurrence network
# Co-occurrence matrix under the form of a dict of dict:
com = { key1:{key2:{'weight':len(dictHashtags[key1].intersection(dictHashtags[key2]))} for key2 in dictHashtags.keys() if (key1!=key2 and bool(dictHashtags[key1].intersection(dictHashtags[key2])))} for key1 in dictHashtags.keys()}
#NB: strange format required by networkx

#NB: itertools.combinations( dictHashtags.keys(), 2) fournirait les bons itérateurs key1, key2, mais ici on veut des trucs plus complexes !

## Reduced network
# # Keep only hashtags with more than 1 occurence
# redHashtags = [ key for key in nbrOccurrences.keys() if nbrOccurrences[key]>1 ]
# 
# redCom = { key1:{key2:{'weight':len(dictHashtags[key1].intersection(dictHashtags[key2]))} for key2 in redHashtags if (key1!=key2 and bool(dictHashtags[key1].intersection(dictHashtags[key2])))} for key1 in redHashtags}

G=nx.from_dict_of_dicts(com)
pos=nx.spring_layout(G, scale = 3)

limit =7
# dictionnary giving the labels
dictLabels = {key:key for key in nbrOccurrences.keys() if nbrOccurrences[key]>limit}
listNodes = list(dictLabels.keys())

listNodesBis = [ key for key in nbrOccurrences.keys() if nbrOccurrences[key]<=limit ]

N = len(listNodes)
colorList=[(2.*x-N)/N for x in range(N)]


# nx.draw(G,pos,with_labels=False, node_size=150, color='b')
nx.draw_networkx_nodes(G, pos, nodelist=listNodesBis, node_size=50, node_color = 'r')
nx.draw_networkx_nodes(G, pos, nodelist=listNodes, node_size=500, node_color=colorList, vmin=-1, vmax = 1, cmap='RdBu_r')
nx.draw_networkx_edges(G, pos)
nx.draw_networkx_labels(G, pos, dictLabels, font_size=10)
plt.savefig(PATH+"graphs\\graph14.png")
plt.show()
        
